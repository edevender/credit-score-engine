package com.db.cse.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.db.cse.entity.CreditRequestRepository;
import com.db.cse.entity.CreditRequestStatus;
import com.db.cse.exception.InvalidRequestException;
import com.db.cse.exception.ProcessException;
import com.db.cse.model.CreditRequest;
import com.db.cse.model.CreditResponse;
import com.db.cse.model.CreditScoreResponse;
import com.db.cse.model.RequestStatus;

/**
 * 
 * @author Devender Service class to invoke the jpa and client services.
 *
 */
@Service
public class CreditEngineService {

	private static Logger LOGGER = LoggerFactory.getLogger(CreditEngineService.class);

	private static final BigDecimal TWO = new BigDecimal(2);

	@Value("${credit.score.engine}")
	private String creditScoreUrl;

	@Value("${credit.score.limit}")
	private int scoreLimit;

	@Autowired
	private CreditRequestRepository repository;
	@Autowired
	private RestTemplate restTemplate;

	public CreditResponse evaluate(CreditRequest request) throws ProcessException {
		CreditRequestStatus status = Optional.ofNullable(validateRequest(request))
				.map(req -> repository.findBySsn(req.getSsn())).orElse(null);
		if (isRequestLessThan30Days(status)) {
			LOGGER.info("A previous request is made less than 30 days.");
			return CreditResponse.build(request, RequestStatus.WAITPERIOD);
		}

		CreditResponse response = evaluateAmount(request);
		LOGGER.info("Loan request is processed. Request status is : {},  Sanctioned amount is : {}",
				response.getStatus(), response.getSanctioAmount());

		return response;
	}

	private CreditResponse evaluateAmount(CreditRequest request) {
		CreditResponse response;

		int creditScore = getCreditScore(request);
		LOGGER.info("Received Credit Score  is : {}", creditScore);

		if (creditScore >= scoreLimit) {
			response = CreditResponse.build(request, calculateSanctionAmount(request.getAmount()),
					RequestStatus.APPROVED);
		} else {
			LOGGER.info("Low credit score {}. The loan application is rejected.", creditScore);
			response = CreditResponse.build(request, RequestStatus.REJECTED);
		}
		repository.save(mapRow(response));

		return response;
	}

	private int getCreditScore(CreditRequest request) {
		CreditScoreResponse creditscore = restTemplate.getForObject(creditScoreUrl, CreditScoreResponse.class,
				request.getSsn());
		return Optional.ofNullable(creditscore).map(CreditScoreResponse::getScore).orElse(0);
	}

	private CreditRequestStatus mapRow(CreditResponse response) {
		CreditRequestStatus status = new CreditRequestStatus();
		status.setSsn(response.getSsn());
		status.setAmount(response.getLoanAmount());
		status.setSanctionAmount(response.getSanctioAmount());
		status.setStatus(response.getStatus());
		status.setLastAccessTime(LocalDateTime.now());
		return status;
	}

	private BigDecimal calculateSanctionAmount(BigDecimal amount) {
		return Optional.ofNullable(amount).map(amt -> amt.divide(TWO)).orElse(BigDecimal.ZERO);
	}

	private boolean isRequestLessThan30Days(CreditRequestStatus status) {
		LocalDateTime time30DaysAgo = LocalDateTime.now().minusDays(30);
		return Optional.ofNullable(status).map(sts -> Optional.ofNullable(sts.getLastAccessTime())
				.map(time -> time.isAfter(time30DaysAgo)).orElse(true)).orElse(false);
	}

	private CreditRequest validateRequest(CreditRequest loan) throws InvalidRequestException {
		Optional.ofNullable(loan)
				.orElseThrow(() -> new InvalidRequestException("Invalid request. Request param is null"));
		Optional.ofNullable(loan.getSsn()).orElseThrow(
				() -> new InvalidRequestException("Invalid request param. SSN number not provided in request"));
		Optional.ofNullable(loan.getAmount()).orElseThrow(
				() -> new InvalidRequestException("Invalid request param. Loan amount is not provided in request"));
		Optional.ofNullable(loan.getIncome()).orElseThrow(
				() -> new InvalidRequestException("Invalid request param. Income is not provided in request"));
		return loan;
	}
}
