package com.db.cse.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.db.cse.model.RequestStatus;

/**
 * 
 * @author Devender
 * Entity class, represents the table credit_score_request_status
 *
 */
@Entity
@Table(name = "credit_score_request_status")
public class CreditRequestStatus implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5329221212944014485L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String ssn;
	private BigDecimal amount;
	private BigDecimal sanctionAmount;
	private RequestStatus status;
	private LocalDateTime lastAccessTime;

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getSanctionAmount() {
		return sanctionAmount;
	}

	public void setSanctionAmount(BigDecimal sanctionAmount) {
		this.sanctionAmount = sanctionAmount;
	}

	public RequestStatus getStatus() {
		return status;
	}

	public void setStatus(RequestStatus status) {
		this.status = status;
	}

	public LocalDateTime getLastAccessTime() {
		return lastAccessTime;
	}

	public void setLastAccessTime(LocalDateTime lastAccessTime) {
		this.lastAccessTime = lastAccessTime;
	}

}
