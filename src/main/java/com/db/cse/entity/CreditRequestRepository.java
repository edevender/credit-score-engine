package com.db.cse.entity;

import java.util.List;

import org.springframework.data.repository.CrudRepository;


/**
 * 
 * @author Devender
 * JPA Repository interface for the table credit_score_request_status
 */
public interface CreditRequestRepository extends CrudRepository<CreditRequestStatus, Long> {
	
	CreditRequestStatus findBySsn(final String ssn);


}
