package com.db.cse.model;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * 
 * @author Devender
 * Represent the request object to map the request body.
 */
public class CreditRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6120331612427433676L;

	@JsonProperty("ssn")
	private String ssn;
	
	@JsonProperty("amount")
	private BigDecimal amount;
	
	@JsonProperty("income")
	private BigDecimal income;

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getIncome() {
		return income;
	}

	public void setIncome(BigDecimal income) {
		this.income = income;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof CreditRequest == false) {
			return false;
		}
		if (this == other) {
			return true;
		}
		final CreditRequest otherObject = (CreditRequest) other;

		return new EqualsBuilder().append(this.ssn, otherObject.ssn).append(this.amount, otherObject.amount)
				.append(this.income, otherObject.income).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.ssn).append(this.amount).append(this.income).toHashCode();
	}

}
