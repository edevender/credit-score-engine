package com.db.cse.model;

/**
 * 
 * @author Devender
 *  Constants represents the Status of the loan request
 *  APPROVED - Loan is Approved
 *  REJECTED - Loan is rejected
 *  WAITPERIOD - A request was made less than 30 Days 
 */
public enum RequestStatus {
	APPROVED,
	REJECTED,
	WAITPERIOD;

}
