package com.db.cse.model;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * 
 * @author Devender
 * Represent the Resonse of API call.
 *
 */
public class CreditResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6120331612427433676L;

	@JsonProperty("ssn")
	private String ssn;
	
	@JsonProperty("loan-amount")
	private BigDecimal loanAmount;
	
	@JsonProperty("sanction-amount")
	private BigDecimal sanctioAmount;
	
	@JsonProperty("request-status")
	private RequestStatus status;

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	
	public BigDecimal getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(BigDecimal loanAmount) {
		this.loanAmount = loanAmount;
	}
	
	
	public BigDecimal getSanctioAmount() {
		return sanctioAmount;
	}

	public void setSanctioAmount(BigDecimal sanctioAmount) {
		this.sanctioAmount = sanctioAmount;
	}

	public RequestStatus getStatus() {
		return status;
	}

	public void setStatus(RequestStatus status) {
		this.status = status;
	}
	
	public static CreditResponse build(CreditRequest request, RequestStatus status) {
		CreditResponse response = new CreditResponse();
		response.setSsn(request.getSsn());
		response.setLoanAmount(request.getAmount());
		response.setStatus(status);
		return response;
	}
	
	public static CreditResponse build(CreditRequest request, BigDecimal sanctionAmount,RequestStatus status) {
		CreditResponse response = new CreditResponse();
		response.setSsn(request.getSsn());
		response.setLoanAmount(request.getAmount());
		response.setSanctioAmount(sanctionAmount);
		response.setStatus(status);
		return response;
	}
	

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof CreditResponse == false) {
			return false;
		}
		if (this == other) {
			return true;
		}
		final CreditResponse otherObject = (CreditResponse) other;

		return new EqualsBuilder()
				.append(this.ssn, otherObject.ssn)
				.append(this.loanAmount, otherObject.loanAmount)
				.append(this.sanctioAmount, otherObject.sanctioAmount)
				.append(this.status, otherObject.status)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(this.ssn)
				.append(this.loanAmount)
				.append(this.sanctioAmount)
				.append(this.status)
				.toHashCode();
	}

}
