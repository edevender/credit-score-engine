package com.db.cse.model;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreditScoreResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6826384678596746631L;
	
	@JsonProperty("ssn")
	private String ssn;
	
	@JsonProperty("score")
	private int score;
	
	@JsonProperty("status")
	private HttpStatus status;
	
	public CreditScoreResponse() {
		//default constructor
	}
	
	public CreditScoreResponse(HttpStatus status) {
		this.status = status;
	}
	
	
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	
	
	
	
	
	

}
