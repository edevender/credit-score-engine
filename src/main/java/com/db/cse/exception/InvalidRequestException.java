package com.db.cse.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="Bad Request. Missing mandatory request parameters")  // 404
public class InvalidRequestException extends ProcessException {
	

	private static final long serialVersionUID = -4670063690722739016L;

	public InvalidRequestException(String message, Throwable e) {
		super(message, e);
		this.message = message;
	}
	
	public InvalidRequestException(String message) {
		super(message);
		this.message = message;
	}

}
