package com.db.cse.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason="Application error. Unable to process the request") 
public class ProcessException extends Exception {
	
	private static final long serialVersionUID = -8871771223981493954L;
	protected String message;
	
	public ProcessException(String message, Throwable e) {
		super(message, e);
		this.message =  message;
	}
	public ProcessException(String message) {
		super(message);
		this.message =  message;
	}
	
	public String getMessage() {
		return message;
	}

}
