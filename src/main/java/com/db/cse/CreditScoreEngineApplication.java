package com.db.cse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.db.cse.controller.CreditEngineController;

@SpringBootApplication
public class CreditScoreEngineApplication {

	public static void main(String[] args) {
		SpringApplication.run(CreditScoreEngineApplication.class, args);
	}

}
