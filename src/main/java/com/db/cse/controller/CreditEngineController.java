package com.db.cse.controller;

import java.time.Duration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.db.cse.exception.ProcessException;
import com.db.cse.model.CreditRequest;
import com.db.cse.model.CreditResponse;
import com.db.cse.service.CreditEngineService;


@RestController
public class CreditEngineController {
	
    private static Logger LOGGER = LoggerFactory.getLogger(CreditEngineController.class);

	
	@Autowired
	private CreditEngineService service;
	
	@GetMapping("/credit-engine/evaluate")
	@ResponseBody
    public CreditResponse evaluate(@RequestBody CreditRequest loan) throws ProcessException {
		
		CreditResponse response =  service.evaluate(loan);
		LOGGER.info("Request for ssn {} is processed successfully",loan.getSsn());
		
        return response;
    }
	
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {	 
	    return builder
	            .setConnectTimeout(Duration.ofMillis(3000))
	            .setReadTimeout(Duration.ofMillis(3000))
	            .build();
	}
}
