package com.db.cse.controller;

import java.util.Optional;

import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.db.cse.model.CreditScoreResponse;



/**
 * @author Devender
 * Class to mock the credit score service. The service is to receive requests for a SSN number and returns with the calculated credit score.
 */
@RestController
public class CreditScoreMockController {
	
    private static Logger LOGGER = LoggerFactory.getLogger(CreditScoreMockController.class);

	
	/**
	 * Method to mock the credit score. 
	 * @param ssn
	 * @return a random integer between 500-999.
	 */
	@GetMapping("/credit-score/{ssn}")
	@ResponseBody
    public CreditScoreResponse evaluate(@PathVariable String ssn) {
		return Optional.ofNullable(ssn).map(this::generateScore).orElseGet(()->invalidResponse());		
    }

	private CreditScoreResponse invalidResponse() {
		LOGGER.error("Invalid request parameters. SSN number is not provided");
		return new CreditScoreResponse(HttpStatus.BAD_REQUEST);
		
	}

	private CreditScoreResponse generateScore(String ssn) {
		CreditScoreResponse response =  new CreditScoreResponse(HttpStatus.OK);
		int score = RandomUtils.nextInt(500, 999);
		LOGGER.info("Credit score generated for SSN {} is {}",ssn, score);
		response.setScore(score);
		response.setSsn(ssn);
		
        return response;
	}

}
