DROP TABLE IF EXISTS credit_score_request_status;

CREATE TABLE credit_score_request_status (
  ssn VARCHAR(250) NOT NULL,
  amount BIGINT NOT NULL,
  sanctionAmount BIGINT NOT NULL,
  status VARCHAR(255) NOT NULL,
  lastAccessTime TIMESTAMP NOT NULL
);

