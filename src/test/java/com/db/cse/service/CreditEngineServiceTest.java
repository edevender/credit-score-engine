package com.db.cse.service;

import static org.hamcrest.CoreMatchers.any;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.matchers.Any;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import com.db.cse.entity.CreditRequestRepository;
import com.db.cse.entity.CreditRequestStatus;
import com.db.cse.exception.ProcessException;
import com.db.cse.model.CreditRequest;
import com.db.cse.model.CreditResponse;
import com.db.cse.model.RequestStatus;

@RunWith(MockitoJUnitRunner.class)
public class CreditEngineServiceTest {
	@InjectMocks
	CreditEngineService service;
	
	@Mock 	
	private CreditRequestRepository repository;
	@Mock
	private RestTemplate restTemplate;
	
	@Test
	public void shouldEvaluateAmount() throws ProcessException {
		String ssn = "AAAA-BBBB-CCCC";
		CreditRequest request = createRequest();
		when(repository.findBySsn(ssn)).thenReturn(createDbRecord(request));		
		CreditResponse response = service.evaluate(createRequest());
		assertNotNull(response);
		assertNotNull(response.getSanctioAmount());
		
	}
	
	@Test
	public void shouldNotEvaluateAmount() throws ProcessException {
		String ssn = "AAAA-BBBB-CCCC";
		CreditRequest request = createRequest();
		when(repository.findBySsn(ssn)).thenReturn(createLatestDbRecord(request));		
		CreditResponse response = service.evaluate(createRequest());
		assertNotNull(response);
		assertNull(response.getSanctioAmount());
		assertEquals(RequestStatus.WAITPERIOD, response.getStatus());
		
	}

	private CreditRequestStatus createDbRecord(CreditRequest req) {
		CreditRequestStatus request = new CreditRequestStatus();
		request.setSsn(req.getSsn());
		request.setLastAccessTime(LocalDateTime.now().minusDays(45));
		request.setAmount(req.getAmount());
		return request;
	}
	private CreditRequestStatus createLatestDbRecord(CreditRequest req) {
		CreditRequestStatus request = new CreditRequestStatus();
		request.setSsn(req.getSsn());
		request.setLastAccessTime(LocalDateTime.now());
		request.setAmount(req.getAmount());
		return request;
	}

	private CreditRequest createRequest() {
		CreditRequest request = new CreditRequest();
		request.setSsn("AAAA-BBBB-CCCC");
		request.setAmount(new BigDecimal(100000));
		request.setIncome(new BigDecimal(15500000));		
		return request;
	}

}
