package com.db.cse;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.net.http.HttpHeaders;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.db.cse.controller.CreditEngineController;
import com.db.cse.controller.CreditScoreMockController;
import com.db.cse.model.CreditRequest;
import com.db.cse.model.CreditResponse;
import com.db.cse.model.CreditScoreResponse;
import com.db.cse.model.RequestStatus;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class CreditScoreEngineApplicationTests {
	
	@LocalServerPort
	private int port;
	
	@Autowired
	CreditEngineController engineController;
	
	@Autowired
	CreditScoreMockController scoreController;
	
	
	@Autowired
	private TestRestTemplate restTemplate;	
	
	
	 

	@Test
	void contextLoads() {
		assertThat(engineController).isNotNull();
		assertThat(scoreController).isNotNull();
	}
	
	@Test
	public void shouldApproveTheLoanRequest() {
		CreditRequest req = new CreditRequest();
		req.setSsn("AAZA-1122-HHH1");
		req.setAmount(new BigDecimal(10000));
		req.setAmount(new BigDecimal(600000));
		
		
		
		HttpEntity<CreditRequest> requestEntity = new HttpEntity<>(req);

		ResponseEntity<CreditResponse> response = restTemplate.exchange("http://localhost:" + port + "/credit-engine/evaluate", HttpMethod.GET, requestEntity, CreditResponse.class);
		assertThat(response).isNotNull();
		assertTrue(response.getBody().getStatus().equals(RequestStatus.APPROVED));
		
		
	}
	
	private CreditScoreResponse mockApprovedResponse(String ssn) {
		CreditScoreResponse response = new CreditScoreResponse(HttpStatus.OK);
		response.setScore(800);
		response.setSsn(ssn);		
		return response;
	}

	@Test
	public void shouldWaitIfMultipleRequestsMadeLessThan30Days() {
		CreditRequest req = new CreditRequest();
		req.setSsn("AAZA-1122-HHH1");
		req.setAmount(new BigDecimal(10000));
		req.setAmount(new BigDecimal(600000));
		
		restTemplate.getForObject("http://localhost:" + port + "/credit-engine/evaluate",
				CreditResponse.class, req);
		
		CreditResponse response = restTemplate.getForObject("http://localhost:" + port + "/credit-engine/evaluate",
				CreditResponse.class, req);
		assertThat(response).isNotNull();
		assertTrue(response.getStatus().equals(RequestStatus.WAITPERIOD));		
		
	}
	
	@Test
	public void shouldRejectIfScoreIsLessThan700() {
		CreditRequest req = new CreditRequest();
		req.setSsn("AAZA-1122-HHH1");
		req.setAmount(new BigDecimal(10000));
		req.setAmount(new BigDecimal(600000));
		restTemplate.getForObject("http://localhost:" + port + "/credit-engine/evaluate",
				CreditResponse.class, req);
		
		CreditResponse response = restTemplate.getForObject("http://localhost:" + port + "/credit-engine/evaluate",
				CreditResponse.class, req);
		assertThat(response).isNotNull();
		assertTrue(response.getStatus().equals(RequestStatus.WAITPERIOD));		
		
	}

}
