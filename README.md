# credit-score-engine

> ## Application to evaluate loan request and compute the eligible amount.
### Application is well integrated with credit score engine and calculates the eligible amount based on the user credit score.

### API end points : 
#### /credit-engine/evaluate
#### Sample Request : {"ssn":"AA124495567","amount":"1020.30","income":"1002555"}
#### Sampple Response : {"ssn":"AA124495567","loan-amount":1020.30,"sanction-amount":510.15,"request-status":"APPROVED"}


